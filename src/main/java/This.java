import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class This {

    public static void main(String[] args) {

        List<Node> nodes = new ArrayList<>();
        nodes.add(createNodeWithNameAndChildren("A", "B", "C"));
        nodes.add(createNodeWithNameAndChildren("B", "C", "D"));
        nodes.add(createNodeWithNameAndChildren("C"));
        nodes.add(createNodeWithNameAndChildren("D"));
        nodes.add(createNodeWithNameAndChildren("E", "F", "G", "H"));
        nodes.add(createNodeWithNameAndChildren("F"));
        nodes.add(createNodeWithNameAndChildren("G", "I", "J"));
        nodes.add(createNodeWithNameAndChildren("H", "K"));
        nodes.add(createNodeWithNameAndChildren("I"));
        nodes.add(createNodeWithNameAndChildren("J"));
        nodes.add(createNodeWithNameAndChildren("K", "L", "M"));
        nodes.add(createNodeWithNameAndChildren("L"));
        nodes.add(createNodeWithNameAndChildren("M"));


        System.out.println("Nodes original:");
        nodes.forEach(node -> System.out.print(node.getName() + "\t"));

        detachNodeWithChildren("E", nodes);
    }

    private static void detachNodeWithChildren(String nodeNameToDetach, List<Node> nodes) {
        Node nodeToDetach = nodes.stream().filter(node -> node.getName().equals(nodeNameToDetach)).findFirst().get();

        List<String> childrenToDetach = getChildrenToDetach(nodeToDetach, nodes);
        System.out.println("\n\nNodes to detach: ");
        childrenToDetach.stream().distinct().forEach(node -> System.out.print(node + "\t"));
    }

    private static List<String> getChildrenToDetach(Node nodeToDetach, List<Node> nodes) {
        List<String> children = new ArrayList<>();
        children.addAll(nodeToDetach.getChildren());

        for (int i = 0; i < nodeToDetach.getChildren().size(); i++) {
            List<Node> childrenNodes = nodeToDetach.getChildren().stream().map(nodeName -> getNodeFromList(nodes, nodeName)).collect(Collectors.toList());

            for (int j = 0; j < childrenNodes.size(); j++) {
                children.addAll(getChildrenToDetach(childrenNodes.get(j), nodes));
            }
        }

        return children;
    }

    private static Node getNodeFromList(List<Node> nodes, String nodeName) {
        return nodes.stream().filter(node -> node.getName().equals(nodeName)).findFirst().get();
    }

    private static Node createNodeWithNameAndChildren(String name, String... children) {
        return new Node(name, new ArrayList<>(Arrays.asList(children)));
    }
}
