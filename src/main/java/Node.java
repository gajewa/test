import java.util.List;

public class Node {

    private String name;

    private List<String> children;

    public Node(String name, List<String> children) {
        this.name = name;
        this.children = children;
    }

    public Node() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getChildren() {
        return children;
    }

    public void setChildren(List<String> children) {
        this.children = children;
    }
}
